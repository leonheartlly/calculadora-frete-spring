# Backend Calculadora

Este é o webservice simples de uma aplicação que efetua calculos e acesso a base de dados.

## Run

Para rodar o projeto, baixe do git, abra o projeto em sua IDE favorita. Espere o build ser concluido e rode o main da classe 'CalculadoraSpringApplication.java'.
Não será necessário nenhum servidor de aplicação, para este caso o tomcat embedded do spring será usado.

## Tecnologias

Para rodar o projeto será necessário que a máquina tenha o Java 8, uma IDE a sua escolha ('eclipse', 'intellij, 'netbeans').
Ao rodar o projeto, tenha certeza de que a porta 8090 não está sendo usada por alguma outra aplicação em sua máquina local.
Caso a porta esteja em uso por outra aplicação, navegue até a pasta resources do projeto e altere a porta no arquivo 'application-dev.properties'.

Para gerenciar as dependências, foi usado o maven. Portanto, caso a maquina já não o tenha instalado,
ao rodar a aplicação, será criado uma pasta oculta denominada '.m2' pasta raiz do usuário, quando não for mais rodar a aplicação, convém remover esta pasta.

A aplicação também faz uso do h2 em memória e do swagger. 

Para a criação do webservice foi usado o Spring-Boot 2.1.1

### H2

Não será necessário banco de dados. Como trata-se apenas de uma POC, foi usado o banco java em memória supracitado.
Este pode ser acessado através do link localhost:8090/h2-console. User: root, pwd: 123.
Note que este é um banco em memória, portanto qualquer alteração feita, não será persistida após derrubar a aplicação e subir novamente.

### Swagger

Para facilitar o uso, verificar endpoints e até mesmo o formato do json esperado, o swagger foi escolhido.
Acesse através do link: localhost:8090/swagger-ui.html

## Como funciona

A tabela de cidade, guarda todas as cidades de origem e destino.
A tabela tipo carga, não está sendo usada, iria usa-la caso sobrasse tempo para modificar o preço de acordo com o tipo da carga (joias, calçados, produtos alimenticios)
A tabela configurações, em um ambiente maior, definiria qual a versão do ambiente está sendo usada sem precisar derrubar a aplicação.
A tabela regra pagamento, define as regras que serão aplicadas no calculo de frete de acordo com a versão do ambiente.
Esta tabela permite que os calculos sejam alterados sem que seja necessário alterar código java ou derrubar aplicação. Assim torna a calculadora um pouco mais dinâmica.

Existe implementado um crud completo destas tabelas.
