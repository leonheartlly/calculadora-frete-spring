package com.app.calculadora.domain.enuns;


import java.util.Optional;


public enum Periculosity {

    NULA( 0, "Nula" ),
    BAIXA( 1, "Baixa" ),
    MEDIA( 2, "Média" ),
    ALTA( 3, "Alta" );

    private int cod;

    private String desc;


    Periculosity(int cod, String desc ) {

        this.cod = cod;
        this.desc = desc;
    }


    public int getCod() {

        return cod;
    }


    public void setCod( int cod ) {

        this.cod = cod;
    }


    public String getDesc() {

        return desc;
    }


    public void setDesc( String desc ) {

        this.desc = desc;
    }


    public static Periculosity getPericulosidade(Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( Periculosity c : Periculosity.values() ) {
            if ( cod.equals( c.getCod() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }


    public static Periculosity getPericulosidade(String desc ) {

        if ( !Optional.ofNullable( desc ).isPresent() ) {
            return null;
        }

        for ( Periculosity c : Periculosity.values() ) {
            if ( desc.equals( c.getDesc() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Descrição inválida: " + desc );
    }
}
