package com.app.calculadora.domain.enuns;


import java.util.Optional;


public enum Status {

    ACTIVE( 0, "Ativo" ),
    INACTIVE( 1, "Inativo" );

    private int cod;

    private String desc;


    Status( int cod, String desc ) {

        this.cod = cod;
        this.desc = desc;
    }


    public int getCod() {

        return cod;
    }


    public void setCod( int cod ) {

        this.cod = cod;
    }


    public String getDesc() {

        return desc;
    }


    public void setDesc( String desc ) {

        this.desc = desc;
    }


    public static Status getStatus( Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( Status c : Status.values() ) {
            if ( cod.equals( c.getCod() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }


    public static Status getStatus( String desc ) {

        if ( !Optional.ofNullable( desc ).isPresent() ) {
            return null;
        }

        for ( Status c : Status.values() ) {
            if ( desc.equals( c.getDesc() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Descrição inválida: " + desc );
    }
}
