package com.app.calculadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
public class State implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String name;

    @JsonIgnore
    @OneToMany( mappedBy = "state" )
    private List< City > cities = new ArrayList<>();


    public State( Long id, String name ) {

        this.id = id;
        this.name = name;
    }


    public State( String name ) {

        this.name = name;
    }


    public State() {

    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public List< City > getCities() {

        return cities;
    }


    public void setCities( List< City > cities ) {

        this.cities = cities;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        State state = (State) o;
        return id.equals( state.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
