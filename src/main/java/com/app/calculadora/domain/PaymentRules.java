package com.app.calculadora.domain;


import com.app.calculadora.domain.enuns.Periculosity;
import com.app.calculadora.domain.enuns.Status;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class PaymentRules {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long versao;

    private boolean status;

    private String periculosidade;

    private Double precoPadrao;

    private String regraCalculo;


    public PaymentRules() {

    }


    public PaymentRules(Long id, Long version, String active, Integer periculosidade, Double precoPadrao, String regraCalculo) {

        this.id = id;
        this.versao = version;
        this.status = active.equals(Status.ACTIVE.getDesc()) ? true : false;
        this.periculosidade = Periculosity.getPericulosidade(periculosidade).getDesc();
        this.precoPadrao = precoPadrao;
        this.regraCalculo = regraCalculo;
    }


    public Long getId() {

        return id;
    }


    public void setId(Long id) {

        this.id = id;
    }


    public Long getVersao() {

        return versao;
    }


    public void setVersao(Long versao) {

        this.versao = versao;
    }


    public boolean isStatus() {

        return status;
    }


    public void setStatus(boolean status) {

        this.status = status;
    }


    public String getPericulosidade() {

        return periculosidade;
    }


    public void setPericulosidade(Integer id) {

        this.periculosidade = Periculosity.getPericulosidade(id).getDesc();
    }


    public Double getPrecoPadrao() {

        return precoPadrao;
    }


    public void setPrecoPadrao(Double precoPadrao) {

        this.precoPadrao = precoPadrao;
    }


    public String getRegraCalculo() {

        return regraCalculo;
    }


    public void setRegraCalculo(String regraCalculo) {

        this.regraCalculo = regraCalculo;
    }
}
