package com.app.calculadora.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class AppConfigurations {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private boolean ativo;

    private Long versao;


    public AppConfigurations() {

    }


    public AppConfigurations(Long id, boolean active, Long versao ) {

        this.id = id;
        this.ativo = active;
        this.versao = versao;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public boolean isAtivo() {

        return ativo;
    }


    public void setAtivo( boolean ativo ) {

        this.ativo = ativo;
    }


    public Long getVersao() {

        return versao;
    }


    public void setVersao( Long versao ) {

        this.versao = versao;
    }
}
