package com.app.calculadora.domain;


import com.app.calculadora.domain.enuns.Periculosity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class City implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String name;

    private String periculosidade;

    @ManyToOne
    @JoinColumn( name = "state_id" )
    private State state;


    public City( Long id, String name, String periculosidade, State state ) {

        this.id = id;
        this.name = name;
        this.periculosidade = periculosidade;
        this.state = state;
    }


    public City() {

    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public State getState() {

        return state;
    }


    public void setState( State state ) {

        this.state = state;
    }


    public String getPericulosidade() {

        return periculosidade;
    }


    public void setPericulosidade( String periculosidade ) {

        this.periculosidade = periculosidade;
    }


    public void convertePericulosidade( Integer id ) {

        this.periculosidade = Periculosity.getPericulosidade( id ).getDesc();
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        City city = (City) o;
        return id.equals( city.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
