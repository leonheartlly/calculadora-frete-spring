package com.app.calculadora.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.app.calculadora.domain.AppConfigurations;
import com.app.calculadora.dto.ConfigurationsDTO;
import com.app.calculadora.repositories.ConfigurationsRepository;
import com.app.calculadora.services.exceptions.DataIntegrityException;
import com.app.calculadora.services.exceptions.ObjectNotFoundException;


@Service
public class ConfigurationsService {

    @Autowired
    private ConfigurationsRepository configurationsRepository;


    public AppConfigurations findById( Long id ) {

        Optional< AppConfigurations > configuracoes = configurationsRepository.findById( id );

        return configuracoes
            .orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + AppConfigurations.class.getName() ) );

    }


    public List< AppConfigurations > listAll() {

        List< AppConfigurations > configuracoes = configurationsRepository.findAll();
        return configuracoes;
    }


    public AppConfigurations insert( AppConfigurations appConfigurations ) {

        appConfigurations.setId( null ); // garantindo que é um objeto novo
        return configurationsRepository.save( appConfigurations );

    }


    public AppConfigurations update( AppConfigurations appConfigurations ) {

        AppConfigurations old = findById( appConfigurations.getId() );

        updateData( old, appConfigurations );
        return configurationsRepository.save( old );
    }


    private void updateData( AppConfigurations old, AppConfigurations newObj ) {

        old.setAtivo( newObj.isAtivo() );
        old.setVersao( newObj.getVersao() );

    }


    public void delete( Long id ) {

        findById( id );
        try {

            configurationsRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "Não foi possível remover este item." );
        }
    }


    public AppConfigurations convertDTO( ConfigurationsDTO configurationsDTO ) {

        return new AppConfigurations( configurationsDTO.getId(), configurationsDTO.getActive().equals( "Ativo" ), configurationsDTO.getVersion() );

    }
}
