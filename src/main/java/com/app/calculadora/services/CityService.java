package com.app.calculadora.services;


import java.util.List;
import java.util.Optional;

import com.app.calculadora.dto.CityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.app.calculadora.domain.City;
import com.app.calculadora.repositories.CityRepository;
import com.app.calculadora.services.exceptions.DataIntegrityException;
import com.app.calculadora.services.exceptions.ObjectNotFoundException;


@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;


    public City findById( Long id ) {

        Optional< City > cities = cityRepository.findById( id );

        return cities.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + City.class.getName() ) );

    }


    public List< City > listAll() {

        List< City > cities = cityRepository.findAll();
        return cities;
    }


    public City insert( City city ) {

        city.setId( null ); // garantindo que é um objeto novo
        return cityRepository.save( city );

    }


    public City update( City newCity ) {

        City old = findById( newCity.getId() );

        updateData( old, newCity );
        return cityRepository.save( old );
    }


    private void updateData( City oldCategory, City newCategory ) {

        oldCategory.setName( newCategory.getName() );

    }


    public void delete( Long id ) {

        findById( id );
        try {

            cityRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "Não foi possível efetuar a remoção." );
        }
    }


    public City convertDTO( CityDTO cityDTO ) {

        return new City( cityDTO.getId(), cityDTO.getName(), cityDTO.getPericulosidade(), cityDTO.getEstado().convertToState( cityDTO.getEstado() ) );

    }
}
