package com.app.calculadora.services;


import java.text.ParseException;
import java.util.Arrays;

import com.app.calculadora.domain.AppConfigurations;
import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.repositories.ConfigurationsRepository;
import com.app.calculadora.repositories.PaymentRulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.calculadora.domain.City;
import com.app.calculadora.domain.State;
import com.app.calculadora.repositories.CityRepository;
import com.app.calculadora.repositories.StateRepository;


@Service
public class DBService {

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private ConfigurationsRepository configurationsRepository;

    @Autowired
    private PaymentRulesRepository paymentRulesRepository;


    public void instantiateDevDataBase()
        throws ParseException {

        AppConfigurations conf1 = new AppConfigurations( null, false, 10L );
        AppConfigurations conf2 = new AppConfigurations( null, true, 11L );

        PaymentRules r1 = new PaymentRules( null, 10L, "Inativo", 1, 20.0D, "*,1" );
        PaymentRules r2 = new PaymentRules( null, 11L, "Ativo", 2, 5.0D, "*,1" );
        PaymentRules r3 = new PaymentRules( null, 11L, "Ativo", 1, 7.0D, "*,2" );
        PaymentRules r4 = new PaymentRules( null, 11L, "Ativo", 3, 10.0D, "*,3" );

        State st = new State( null, "MG" );
        State st2 = new State( null, "SP" );
        State st3 = new State( null, "SC" );

        City ct = new City( null, "Uberlandia", "Baixa", st );
        City ct2 = new City( null, "São paulo", "Alta", st2 );
        City ct3 = new City( null, "Blumenau", "Média", st3 );

        st.getCities().addAll( Arrays.asList( ct ) );
        st.getCities().addAll( Arrays.asList( ct2, ct3 ) );

        stateRepository.saveAll( Arrays.asList( st, st2, st3 ) );

        cityRepository.saveAll( Arrays.asList( ct, ct2, ct3 ) );

        configurationsRepository.saveAll( Arrays.asList( conf1, conf2 ) );

        paymentRulesRepository.saveAll( Arrays.asList( r1, r2, r3, r4 ) );

    }
}
