package com.app.calculadora.services;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.app.calculadora.domain.City;
import com.app.calculadora.domain.AppConfigurations;
import com.app.calculadora.domain.enuns.Periculosity;
import com.app.calculadora.dto.PaymentRulesInsertDTO;
import com.app.calculadora.repositories.CityRepository;
import com.app.calculadora.repositories.ConfigurationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.dto.PaymentRulesDTO;
import com.app.calculadora.repositories.PaymentRulesRepository;
import com.app.calculadora.services.exceptions.DataIntegrityException;
import com.app.calculadora.services.exceptions.ObjectNotFoundException;


@Service
public class PaymentRulesService {

    @Autowired
    private PaymentRulesRepository paymentRulesRepository;

    @Autowired
    private ConfigurationsRepository configurationsRepository;

    @Autowired
    private CityRepository cityRepository;


    public PaymentRules findById( Long id ) {

        Optional< PaymentRules > regraPagamento = paymentRulesRepository.findById( id );

        return regraPagamento
            .orElseThrow( () -> new ObjectNotFoundException( "Objeto não encontrado. Id: " + id + ", Tipo: " + PaymentRules.class.getName() ) );

    }


    public List< PaymentRules > listAll() {

        List< PaymentRules > paymentRules = paymentRulesRepository.findAll();

        return paymentRules;
    }


    /**
     * Efetua insert de regra de pagamento.
     *
     * @param paymentRules
     *            regra a ser inserida
     * @return regra inserida.
     */
    @Transactional
    public PaymentRules insert( PaymentRules paymentRules ) {

        paymentRules.setId( null ); // garantindo que é um objeto novo
        paymentRules = paymentRulesRepository.save( paymentRules );
        return paymentRules;

    }


    public PaymentRules update( PaymentRules newObj ) {

        PaymentRules old = findById( newObj.getId() );

        updateData( old, newObj );
        return paymentRulesRepository.save( old );
    }


    private void updateData( PaymentRules old, PaymentRules newObj ) {

        // TODO arrumar isso
        // old.setPericulosity( newObj.getPericulosity() );
        old.setPrecoPadrao( newObj.getPrecoPadrao() );
        old.setVersao( newObj.getVersao() );
        old.setStatus( newObj.isStatus() );
        old.setRegraCalculo( newObj.getRegraCalculo() );

    }


    public void delete( Long id ) {

        findById( id );
        try {

            paymentRulesRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "Erro ao remover o objeto selecionado." );
        }
    }


    /**
     * Realiza operações necessárias para efetuar o calculo do valor do frete.
     * @param originId cidade origem.
     * @param destinationId cidade destino.
     * @param weight peso.
     * @return Valor calculado.
     */
    public Double findPrice( Long originId, Long destinationId, Double weight ) {

        Optional< City > originCity = cityRepository.findById( originId );
        Optional< City > destinationCity = cityRepository.findById( destinationId );

        // TODO Optional
        AppConfigurations conf = configurationsRepository.findByAtivo( true );
        String periculosity = findPericulosity(
            Periculosity.getPericulosidade( originCity.get().getPericulosidade() ),
            Periculosity.getPericulosidade( destinationCity.get().getPericulosidade() ) );

        // TODO optional
        PaymentRules regra = paymentRulesRepository.findOneByVersaoAndStatusAndPericulosidade( conf.getVersao(), true, periculosity );

        return ruleInterpreterCalc( regra, weight );
    }


    /**
     * Busca e realiza a operação de acordo com a regra encontrada.
     * @param rule regra buscada na base.
     * @param weight Peso da mercadoria.
     * @return Valor calculado.
     */
    public Double ruleInterpreterCalc( PaymentRules rule, Double weight ) {

        String[] operation = rule.getRegraCalculo().split( "," );
        weight = weight * rule.getPrecoPadrao();

        Double calculatedValue = Double.parseDouble( operation[ 1 ] );

        switch ( operation[ 0 ] ) {
            case "*":
                calculatedValue *= weight;
                break;
            case "/":
                calculatedValue /= weight;
                break;
            case "+":
                calculatedValue += weight;
                break;
            case "-":
                calculatedValue -= weight;
                break;
            default:
                calculatedValue = 0D;
                break;
        }

        return calculatedValue;

    }


    public String findPericulosity( Periculosity origin, Periculosity destination ) {

        return origin.getCod() > destination.getCod() ? origin.getDesc() : destination.getDesc();
    }


    public PaymentRules convertDTO( PaymentRulesDTO dto ) {

        return new PaymentRules( null, dto.getVersion(), dto.getStatus(), Periculosity.getPericulosidade( dto.getPericulosity() ).getCod(),
            dto.getDefaultPrice(), dto.getCalcRule() );
    }


    public PaymentRules convertDTO( PaymentRulesInsertDTO dto ) {

        return new PaymentRules( null, dto.getVersion(), dto.getStatus(), Periculosity.getPericulosidade( dto.getPericulosity() ).getCod(),
            dto.getDefaultPrice(), dto.getRuleCalc() );
    }

}
