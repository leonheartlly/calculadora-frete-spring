package com.app.calculadora.services;


import java.util.List;
import java.util.Optional;

import com.app.calculadora.dto.StateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.app.calculadora.domain.State;
import com.app.calculadora.repositories.StateRepository;
import com.app.calculadora.services.exceptions.DataIntegrityException;
import com.app.calculadora.services.exceptions.ObjectNotFoundException;


@Service
public class StateService {

    @Autowired
    private StateRepository stateRepository;


    public State findById( Long id ) {

        Optional< State > estado = stateRepository.findById( id );

        return estado.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + State.class.getName() ) );

    }


    public List< State > listAll() {

        List< State > states = stateRepository.findAll();
        return states;
    }


    public State insert( State state ) {

        state.setId( null );
        return stateRepository.save( state );

    }


    public State update( State state ) {

        State old = findById( state.getId() );

        updateData( old, state );
        return stateRepository.save( old );
    }


    private void updateData( State old, State newObj ) {

        old.setName( newObj.getName() );

    }


    public void delete( Long id ) {

        findById( id );
        try {

            stateRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "Não foi possível remover." );
        }
    }


    public State convertDTO( StateDTO estadoDTO ) {

        State state = new State();
        state.setName( estadoDTO.getName() );

        return state;
    }

}
