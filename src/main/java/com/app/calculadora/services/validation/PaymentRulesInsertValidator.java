package com.app.calculadora.services.validation;


import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.domain.enuns.Periculosity;
import com.app.calculadora.domain.enuns.Status;
import com.app.calculadora.dto.PaymentRulesInsertDTO;
import com.app.calculadora.repositories.PaymentRulesRepository;
import com.app.calculadora.resources.exception.FieldMessage;


public class PaymentRulesInsertValidator implements ConstraintValidator< PaymentRulesInsert, PaymentRulesInsertDTO > {

    @Autowired
    private PaymentRulesRepository paymentRulesRepository;


    @Override
    public void initialize( PaymentRulesInsert ann ) {

    }


    @Override
    public boolean isValid( PaymentRulesInsertDTO objVO, ConstraintValidatorContext context ) {

        List< FieldMessage > list = new ArrayList<>();

        if ( !ObjectUtils.isEmpty( objVO ) ) {

            if ( !( objVO.getStatus().equals( Status.ACTIVE.getDesc() ) || objVO.getStatus().equals( Status.INACTIVE.getDesc() ) ) ) {
                list.add( new FieldMessage( "status", "Status inválido." ) );
            }

            if ( !( objVO.getPericulosity().equals( Periculosity.NULA.getDesc() ) || objVO.getPericulosity().equals( Periculosity.BAIXA.getDesc() )
                || objVO.getPericulosity().equals( Periculosity.MEDIA.getDesc() ) || objVO.getPericulosity().equals( Periculosity.ALTA.getDesc() ) ) ) {
                list.add( new FieldMessage( "periculosity", "Periculosity inválida." ) );
            }

            PaymentRules aux = paymentRulesRepository.findOneByStatus( true );

            if ( aux != null && objVO.getVersion() < aux.getVersao() ) {

                list.add( new FieldMessage( "version", "Versão deve ser maior que a versão ativa anterior." ) );
            }

            if ( StringUtils.isNotEmpty( objVO.getRuleCalc() ) && !objVO.getRuleCalc().contains( "," ) ) {
                list.add( new FieldMessage( "ruleCalc", "A regra deve conter pelo menos uma vírgula. Padrão [OPERAND],[VALUE]." ) );
            }

            if ( objVO.getDefaultPrice() != null && objVO.getDefaultPrice() < 0D ) {
                list.add( new FieldMessage( "defaultPrice", "O valor padrão deve ser superior a 0." ) );
            }
        } else {
            list.add( new FieldMessage( "id", "O objeto não pode ser vazio." ) );
        }

        for ( FieldMessage e : list ) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }

        return list.isEmpty();
    }
}
