package com.app.calculadora.services.validation;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.dto.PaymentRulesDTO;
import com.app.calculadora.repositories.PaymentRulesRepository;
import com.app.calculadora.resources.exception.FieldMessage;


public class PaymentRulesUpdateValidator implements ConstraintValidator< PaymentRulesUpdate, PaymentRulesDTO > {

    @Autowired
    private PaymentRulesRepository paymentRulesRepository;

    @Autowired
    private HttpServletRequest httpServletRequest;


    @Override
    public void initialize( PaymentRulesUpdate ann ) {

    }


    @Override
    public boolean isValid( PaymentRulesDTO objVO, ConstraintValidatorContext context ) {

        Map< String, String > map = (Map< String, String >) httpServletRequest.getAttribute( HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE );
        Long uriId = Long.parseLong( map.get( "id" ) );

        List< FieldMessage > list = new ArrayList<>();

        Optional< PaymentRules > aux = paymentRulesRepository.findById( uriId );

        if ( !aux.isPresent() ) {
            list.add( new FieldMessage( "id", "Regra de Pagamento não encontrada." ) );
        } else {

            if ( objVO.getDefaultPrice() < 0D ) {
                list.add( new FieldMessage( "defaultPrice", "Preco padrao deve ser maior que 0." ) );
            }

            if ( objVO.getVersion() < aux.get().getVersao() ) {
                list.add( new FieldMessage( "version", "A versao não pode ser inferior à versão anterior." ) );
            }

            if ( StringUtils.isEmpty( objVO.getCalcRule() ) ) {
                list.add( new FieldMessage( "ruleCalc", "A regra de calculo não pode ser vazia." ) );
            } else if ( !objVO.getCalcRule().contains( "," ) ) {
                list.add( new FieldMessage( "ruleCalc", "A regra de calculo deve estar no padrão [OPERAND],[VALUE]." ) );
            }

            if ( StringUtils.isEmpty( objVO.getStatus() ) || !( objVO.getStatus().equals( "Ativo" ) || objVO.getStatus().equals( "Inativo" ) ) ) {
                list.add( new FieldMessage( "status", "Valor incorreto para o status." ) );
            }
        }

        for ( FieldMessage e : list ) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }
        return list.isEmpty();
    }
}
