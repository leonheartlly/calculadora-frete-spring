package com.app.calculadora.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.calculadora.domain.AppConfigurations;


@Repository
public interface ConfigurationsRepository extends JpaRepository<AppConfigurations, Long > {

    AppConfigurations findByAtivo(boolean ativo );
}
