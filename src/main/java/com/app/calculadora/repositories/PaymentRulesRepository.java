package com.app.calculadora.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.calculadora.domain.PaymentRules;


@Repository
public interface PaymentRulesRepository extends JpaRepository<PaymentRules, Long > {

    PaymentRules findOneByVersaoAndStatusAndPericulosidade(Long versao, boolean status, String periculosidade );


    PaymentRules findOneByStatus(boolean status );

}
