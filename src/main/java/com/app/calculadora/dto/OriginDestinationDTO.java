package com.app.calculadora.dto;


import java.io.Serializable;


public class OriginDestinationDTO implements Serializable {

    private Long origin;

    private Long destination;

    private Double weight;


    public OriginDestinationDTO() {

    }


    public OriginDestinationDTO( Long origemid, Long destinoid, Double weight ) {

        this.origin = origemid;
        this.destination = destinoid;
        this.weight = weight;
    }


    public Long getOrigin() {

        return origin;
    }


    public void setOrigin( Long origin ) {

        this.origin = origin;
    }


    public Long getDestination() {

        return destination;
    }


    public void setDestination( Long destination ) {

        this.destination = destination;
    }


    public Double getWeight() {

        return weight;
    }


    public void setWeight( Double weight ) {

        this.weight = weight;
    }
}
