package com.app.calculadora.dto;


import java.io.Serializable;

import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.services.validation.PaymentRulesUpdate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;


@PaymentRulesUpdate
public class PaymentRulesDTO implements Serializable {

    private Long id;

    private Long version;

    @NotEmpty( message = "Campo Obrigatório." )
    private String status;

    @NotEmpty( message = "Campo Obrigatório." )
    private String periculosity;

    private Double defaultPrice;

    @NotEmpty( message = "Campo Obrigatório." )
    @Length( min = 3, message = "O tamanho mínimo de uma regra é 3 caracteres." )
    private String calcRule;


    public PaymentRulesDTO(
        Long id,
        Long version,
        @NotEmpty( message = "Campo Obrigatório." ) String status,
        @NotEmpty( message = "Campo Obrigatório." ) String periculosity,
        Double defaultPrice,
        @NotEmpty( message = "Campo Obrigatório." ) @Length( min = 3, message = "O tamanho mínimo de uma regra é 3 caracteres." ) String calcRule ) {

        this.id = id;
        this.version = version;
        this.status = status;
        this.periculosity = periculosity;
        this.defaultPrice = defaultPrice;
        this.calcRule = calcRule;
    }


    public PaymentRulesDTO() {

    }


    public PaymentRulesDTO( PaymentRules paymentRules ) {

        this.id = paymentRules.getId();
        this.version = paymentRules.getVersao();
        this.status = paymentRules.isStatus() == true ? "Ativo" : "Inativo";
        this.periculosity = paymentRules.getPericulosidade();
        this.defaultPrice = paymentRules.getPrecoPadrao();
        this.calcRule = paymentRules.getRegraCalculo();
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Long getVersion() {

        return version;
    }


    public void setVersion( Long version ) {

        this.version = version;
    }


    public String getStatus() {

        return status;
    }


    public void setStatus( String status ) {

        this.status = status;
    }


    public String getPericulosity() {

        return periculosity;
    }


    public void setPericulosity( String periculosity ) {

        this.periculosity = periculosity;
    }


    public Double getDefaultPrice() {

        return defaultPrice;
    }


    public void setDefaultPrice( Double defaultPrice ) {

        this.defaultPrice = defaultPrice;
    }


    public String getCalcRule() {

        return calcRule;
    }


    public void setCalcRule( String calcRule ) {

        this.calcRule = calcRule;
    }
}
