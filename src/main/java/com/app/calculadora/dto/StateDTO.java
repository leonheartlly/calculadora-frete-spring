package com.app.calculadora.dto;


import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.app.calculadora.domain.State;


public class StateDTO implements Serializable {

    @NotEmpty( message = "Preenchimento obrigatório." )
    @Length( min = 2, max = 3, message = "O estado deve ter entre 2 e 3 caracteres." )
    private String name;


    public StateDTO() {

    }


    public StateDTO( State state ) {

        name = state.getName();
    }

    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }

    public State convertToState(StateDTO stateDTO){
            return new State( stateDTO.getName()  );
    }
}
