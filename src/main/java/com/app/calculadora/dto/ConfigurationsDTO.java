package com.app.calculadora.dto;


import java.io.Serializable;

import com.app.calculadora.domain.AppConfigurations;


public class ConfigurationsDTO implements Serializable {

    private Long id;

    private String active;

    private Long version;


    public ConfigurationsDTO() {

    }


    public ConfigurationsDTO( AppConfigurations appConfigurations ) {

        this.id = appConfigurations.getId();
        this.active = appConfigurations.isAtivo() == true ? "Ativo" : "Inativo";
        this.version = appConfigurations.getVersao();
    }


    public ConfigurationsDTO( Long id, String active, Long version ) {

        this.id = id;
        this.active = active;
        this.version = version;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getActive() {

        return active;
    }


    public void setActive( String active ) {

        this.active = active;
    }


    public Long getVersion() {

        return version;
    }


    public void setVersion( Long version ) {

        this.version = version;
    }
}
