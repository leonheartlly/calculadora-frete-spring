package com.app.calculadora.dto;


import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.services.validation.PaymentRulesInsert;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


@PaymentRulesInsert
public class PaymentRulesInsertDTO implements Serializable {

    private Long version;

    @NotEmpty( message = "Campo Obrigatório." )
    private String status;

    @NotEmpty( message = "Campo Obrigatório." )
    private String periculosity;

    private Double defaultPrice;

    @NotEmpty( message = "Campo Obrigatório." )
    @Length( min = 3, message = "O tamanho mínimo de uma regra é 3 caracteres." )
    private String ruleCalc;


    public PaymentRulesInsertDTO(
        Long id,
        Long version,
        @NotEmpty( message = "Campo Obrigatório." ) String status,
        @NotEmpty( message = "Campo Obrigatório." ) String periculosity,
        Double defaultPrice,
        @NotEmpty( message = "Campo Obrigatório." ) @Length( min = 3, message = "O tamanho mínimo de uma regra é 3 caracteres." ) String ruleCalc ) {

        this.version = version;
        this.status = status;
        this.periculosity = periculosity;
        this.defaultPrice = defaultPrice;
        this.ruleCalc = ruleCalc;
    }


    public PaymentRulesInsertDTO() {

    }


    public PaymentRulesInsertDTO( PaymentRules paymentRules ) {

        this.version = paymentRules.getVersao();
        this.status = paymentRules.isStatus() == true ? "Ativo" : "Inativo";
        this.periculosity = paymentRules.getPericulosidade();
        this.defaultPrice = paymentRules.getPrecoPadrao();
        this.ruleCalc = paymentRules.getRegraCalculo();
    }


    public Long getVersion() {

        return version;
    }


    public void setVersion( Long version ) {

        this.version = version;
    }


    public String getStatus() {

        return status;
    }


    public void setStatus( String status ) {

        this.status = status;
    }


    public String getPericulosity() {

        return periculosity;
    }


    public void setPericulosity( String periculosity ) {

        this.periculosity = periculosity;
    }


    public Double getDefaultPrice() {

        return defaultPrice;
    }


    public void setDefaultPrice( Double defaultPrice ) {

        this.defaultPrice = defaultPrice;
    }


    public String getRuleCalc() {

        return ruleCalc;
    }


    public void setRuleCalc( String ruleCalc ) {

        this.ruleCalc = ruleCalc;
    }
}
