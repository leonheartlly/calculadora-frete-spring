package com.app.calculadora.dto;


import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.app.calculadora.domain.City;
import com.app.calculadora.domain.State;


public class CityDTO implements Serializable {

    private Long id;

    @NotEmpty( message = "Cidade é de preenchimento obrigatório." )
    @Length( min = 5, max = 15, message = "Cidade deve ter entre 5 e 15 caracteres." )
    private String name;

    @NotEmpty( message = "Periculosity é de preenchimento obrigatório." )
    private String periculosidade;

    private StateDTO estado;


    public CityDTO() {

    }


    public CityDTO(
        Long id,
        @NotEmpty( message = "Cidade é de preenchimento obrigatório." ) @Length( min = 5, max = 15,
            message = "Cidade deve ter entre 5 e 15 caracteres." ) String name,
        @NotEmpty( message = "Periculosity é de preenchimento obrigatório." ) String periculosidade,
        StateDTO estado ) {

        this.id = id;
        this.name = name;
        this.periculosidade = periculosidade;
        this.estado = estado;
    }


    public CityDTO( City city ) {

        this.id = city.getId();
        this.name = city.getName();
        this.periculosidade = city.getPericulosidade();
        this.estado = new StateDTO( city.getState() );
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getPericulosidade() {

        return periculosidade;
    }


    public void setPericulosidade( String periculosidade ) {

        this.periculosidade = periculosidade;
    }


    public StateDTO getEstado() {

        return estado;
    }


    public void setEstado( StateDTO estado ) {

        this.estado = estado;
    }
}
