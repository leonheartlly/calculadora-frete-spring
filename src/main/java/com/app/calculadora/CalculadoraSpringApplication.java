package com.app.calculadora;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CalculadoraSpringApplication implements CommandLineRunner {




    public static void main( String[] args ) {

        SpringApplication.run( CalculadoraSpringApplication.class, args );
    }


    /**
     * Criado apenas para adicionar objetos toda vez que subir o banco e facilitar
     * os testes
     * 
     * @param args
     * @throws Exception
     */
    @Override
    public void run( String... args )
        throws Exception {



    }
}
