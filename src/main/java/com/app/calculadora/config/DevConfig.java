package com.app.calculadora.config;


import com.app.calculadora.services.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;


@Configuration
@Profile( "dev" )
public class DevConfig {

    @Autowired
    private DBService dbService;


    @Bean
    public boolean instanteateDataBase()
        throws ParseException {

        dbService.instantiateDevDataBase();
        return true;
    }
}
