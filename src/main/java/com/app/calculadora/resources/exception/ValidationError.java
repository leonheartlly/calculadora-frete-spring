package com.app.calculadora.resources.exception;


import java.util.ArrayList;
import java.util.List;


public class ValidationError extends StandardError {

    private List< FieldMessage > fields = new ArrayList<>();


    public ValidationError( Integer status, String msg, Long timeStamp ) {

        super( status, msg, timeStamp );
    }


    public List< FieldMessage > getErrors() {

        return fields;
    }


    public void addError( String fieldName, String msg ) {

        fields.add( new FieldMessage( fieldName, msg ) );
    }
}
