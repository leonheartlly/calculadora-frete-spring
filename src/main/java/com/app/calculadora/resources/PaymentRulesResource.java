package com.app.calculadora.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.app.calculadora.dto.OriginDestinationDTO;
import com.app.calculadora.dto.PaymentRulesInsertDTO;
import com.app.calculadora.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.calculadora.domain.PaymentRules;
import com.app.calculadora.dto.PaymentRulesDTO;
import com.app.calculadora.services.PaymentRulesService;


@RestController
@RequestMapping( value = "/regras" )
public class PaymentRulesResource {

    @Autowired
    private PaymentRulesService paymentRulesService;

    @Autowired
    private CityService cityService;


    @RequestMapping( method = RequestMethod.GET )
    public List< PaymentRulesDTO > listAll() {

        List< PaymentRules > regras = paymentRulesService.listAll();
        List< PaymentRulesDTO > regraDTO = regras.stream().map( obj -> new PaymentRulesDTO( obj ) ).collect( Collectors.toList() );
        return regraDTO;
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Long id ) {

        PaymentRules paymentRules = paymentRulesService.findById( id );
        PaymentRulesDTO paymentRulesDTO = new PaymentRulesDTO( paymentRules );
        return ResponseEntity.ok().body( paymentRulesDTO );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody PaymentRulesInsertDTO paymentRulesInsertDTO ) {

        PaymentRules paymentRules = paymentRulesService.convertDTO( paymentRulesInsertDTO );
        paymentRules = paymentRulesService.insert( paymentRules );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( paymentRules.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > update( @Valid @RequestBody PaymentRulesDTO paymentRulesDTO, @PathVariable Long id ) {

        PaymentRules paymentRules = paymentRulesService.convertDTO( paymentRulesDTO );
        paymentRules.setId( id );
        paymentRules = paymentRulesService.update( paymentRules );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > delete( @PathVariable Long id ) {

        paymentRulesService.delete( id );
        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/calculate", method = RequestMethod.POST )
    public ResponseEntity< Double > calculate( @Valid @RequestBody OriginDestinationDTO dto ) {

        Double price = paymentRulesService.findPrice( dto.getOrigin(), dto.getDestination(), dto.getWeight() );
        return ResponseEntity.ok().body( price );
    }

}
