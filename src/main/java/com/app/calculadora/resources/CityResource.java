package com.app.calculadora.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.app.calculadora.dto.CityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.calculadora.domain.City;
import com.app.calculadora.services.CityService;


@RestController
@RequestMapping( value = "/cidades" )
public class CityResource {

    @Autowired
    private CityService cityService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< CityDTO > > listAll() {

        List< City > cidades = cityService.listAll();
        List< CityDTO > cidadesDTO = cidades.stream().map( obj -> new CityDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( cidadesDTO );
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Long id ) {

        City cidade = cityService.findById( id );
        CityDTO cidadeDTO = new CityDTO( cidade );

        return ResponseEntity.ok().body( cidadeDTO );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody CityDTO cityDTO ) {

        City cidade = cityService.convertDTO( cityDTO );
        cidade = cityService.insert( cidade );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( cidade.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > update( @Valid @RequestBody CityDTO cityDTO, @PathVariable Long id ) {

        City cidade = cityService.convertDTO( cityDTO );
        cidade.setId( id );
        cidade = cityService.update( cidade );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > delete( @PathVariable Long id ) {

        cityService.delete( id );
        return ResponseEntity.noContent().build();
    }

}
