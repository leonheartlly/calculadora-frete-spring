package com.app.calculadora.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.calculadora.domain.State;
import com.app.calculadora.dto.StateDTO;
import com.app.calculadora.services.StateService;


@RestController
@RequestMapping( value = "/estados" )
public class StateResource {

    @Autowired
    private StateService stateService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< StateDTO > > listAll() {

        List< State > estados = stateService.listAll();
        List< StateDTO > estadosDTO = estados.stream().map( obj -> new StateDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( estadosDTO );
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Long id ) {

        State estado = stateService.findById( id );
        return ResponseEntity.ok().body( estado );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody StateDTO estadoDTO ) {

        State category = stateService.convertDTO( estadoDTO );
        category = stateService.insert( category );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( category.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > update( @Valid @RequestBody StateDTO estadoDTO, @PathVariable Long id ) {

        State estado = stateService.convertDTO( estadoDTO );
        estado.setId( id );
        estado = stateService.update( estado );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > delete( @PathVariable Long id ) {

        stateService.delete( id );
        return ResponseEntity.noContent().build();
    }

}
