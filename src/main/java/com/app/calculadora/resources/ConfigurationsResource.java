package com.app.calculadora.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.calculadora.domain.AppConfigurations;
import com.app.calculadora.dto.ConfigurationsDTO;
import com.app.calculadora.services.ConfigurationsService;


@RestController
@RequestMapping( value = "/configuracoes" )
public class ConfigurationsResource {

    @Autowired
    private ConfigurationsService configurationsService;


    @RequestMapping( method = RequestMethod.GET )
    public List< ConfigurationsDTO > listAll() {

        List< AppConfigurations > configuracoes = configurationsService.listAll();
        List< ConfigurationsDTO > configurationsDTOS = configuracoes.stream().map( obj -> new ConfigurationsDTO( obj ) ).collect( Collectors.toList() );
        return configurationsDTOS;
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Long id ) {

        AppConfigurations appConfigurations = configurationsService.findById( id );
        ConfigurationsDTO configurationsDTO = new ConfigurationsDTO( appConfigurations );
        return ResponseEntity.ok().body( configurationsDTO );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody ConfigurationsDTO configurationsDTO ) {

        AppConfigurations appConfigurations = configurationsService.convertDTO( configurationsDTO );
        appConfigurations = configurationsService.insert( appConfigurations );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( appConfigurations.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > update( @Valid @RequestBody ConfigurationsDTO configurationsDTO, @PathVariable Long id ) {

        AppConfigurations appConfigurations = configurationsService.convertDTO( configurationsDTO );
        appConfigurations.setId( id );
        appConfigurations = configurationsService.update( appConfigurations );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > delete( @PathVariable Long id ) {

        configurationsService.delete( id );
        return ResponseEntity.noContent().build();
    }

}
